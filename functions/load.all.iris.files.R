ipak(parallel)
ipak(gtools)
ipak(data.table)
ipak(taRifx) #for stack



load.all.iris.files <- function(folder.path, 
                                read.dat.files=F, 
                                parallel=F, 
                                cores.to.use=2, 
                                filename.mode = "auto", 
                                recursive=F,
                                include.full.path=F){
  
  if(read.dat.files){
    filenames = mixedsort(list.files(folder.path, glob2rx("*.dat"), full.names = TRUE, recursive=recursive))
  } else {
    filenames = mixedsort(list.files(folder.path, glob2rx("*.iris"), full.names = TRUE, recursive=recursive))
  }
  
  if(!parallel){
    all.iris.files.data.list = lapply(filenames, load.one.iris.file2, filename.mode, include.full.path)
  } else {
    all.iris.files.data.list = mclapply(filenames, load.one.iris.file2,filename.mode, include.full.path, mc.cores = cores.to.use)
  }
  
  if(length(all.iris.files.data.list)!=0){
    #get the list of loaded tables and make a big table by stacking them on top of each other
    #cat(paste0(length(all.iris.files.data.list), " files in folder\n"))
    all.iris.files.data = rbindlist(all.iris.files.data.list, fill=T) #rbind(all.iris.files.data.list, this.iris.file.data)
  } else {
    all.iris.files.data = NULL
  }
  
  return(data.table(all.iris.files.data))
}


load.one.iris.file2 <-function(filename, filename.mode="auto", include.full.path = F){
  tryCatch(
    {
      this.iris.file.data = loadIrisFile.size.add.meta.info(filename, filename.mode)
      
      if(include.full.path){
        this.iris.file.data$file.path=filename
      }
      
      #check if file is correct
      number.of.rows.in.file = dim(this.iris.file.data)[1]
      if(!(number.of.rows.in.file==6144 || number.of.rows.in.file==1536 || number.of.rows.in.file==384 || number.of.rows.in.file==96)){
        this.iris.file.data=NULL
        warning(paste0("could not load file ", basename(filename)))
      }
      this.iris.file.data = data.table(this.iris.file.data, key="unique.mutant.id") #return statement
      this.iris.file.data
    }, error=function(cond) {
      warning(paste0("could not load file ", basename(filename)))
      NULL#return statement
    }
  )
}

# 
# 
# load.one.iris.file <-function(filenames, i){
#   tryCatch(
# {
#   this.iris.file.data = loadIrisFile.size.add.meta.info(filenames[i])
#   
#   #check if file is correct
#   number.of.rows.in.file = dim(this.iris.file.data)[1]
#   if(!(number.of.rows.in.file==6144 || number.of.rows.in.file==1536 || number.of.rows.in.file==384 || number.of.rows.in.file==96)){
#     this.iris.file.data=NULL
#     warning(paste0("could not load file ", basename(filenames[i])))
#   }
#   this.iris.file.data #return statement
# }, error=function(cond) {
#   warning(paste0("could not load file ", basename(filenames[i])))
#   NULL#return statement
# }
#   )
# }




get.iris.plate.info <- function(file.path, mode="auto"){
  #This function gets you all the plate information available through the filename
  
  just.filename = basename(file.path)
  
  
  if(mode=="dont care"){
    iris.plate.info = data.frame(plate.number = 0, 
                                 replicate.number = "", 
                                 chemical = "",
                                 condition = "",
                                 filename = just.filename)
  }
  
  
  
  if(mode=="auto"){
    
    mode = str_count(just.filename, "-")
    
    if(length(grep("^BS",just.filename, value = F))) #if filename starts with BS
      mode="BS" #B. subtilis
    
    if(length(grep("^Ca_",just.filename, value = F))) #if filename starts with Ca_
      mode="Ca" #C. albicans
    
    
    if(length(grep("^timecourse",just.filename, value = F))) #if filename starts with plate_0
      mode="timecourse2" #timecourse data from the experiment done in August 2015
  }
  
  
  #modern naming style
  #Cephalexin-1-5_A.JPG
  if(mode==2){
    plate.split = as.vector(unlist(strsplit(just.filename, split="-")))
    plate.split.again = as.vector(unlist(strsplit(plate.split[3], split="_")))
    plate.split.yet.again = as.vector(unlist(strsplit( plate.split.again[2], split="\\.")))
    
    
    
    iris.plate.info = data.frame(plate.number = as.integer(plate.split.again[1]), 
                                 replicate.number = plate.split.yet.again[1], 
                                 chemical = plate.split[1],
                                 condition = paste(plate.split[1], plate.split[2], sep="-"),
                                 filename = just.filename)
  }
  
  #UCSF naming style
  if(mode==1){
    plate.split = as.vector(unlist(strsplit(just.filename, split="-")))
    plate.split.again = as.vector(unlist(strsplit(plate.split[2], split="_")))
    plate.split.yet.again = as.vector(unlist(strsplit( plate.split.again[2], split="\\.")))
    
    
    
    iris.plate.info = data.frame(plate.number = as.integer(plate.split.again[1]), 
                                 replicate.number = plate.split.yet.again[1], 
                                 chemical = plate.split[1],
                                 condition = plate.split[1],
                                 filename = just.filename)
  }
  
  #timecourse data
  #this can be either:
  #9h30_P_001.JPG.iris --> normal 1536
  #9h30_001.JPG.iris --> normal 384
  #13h27wP_001.JPG.iris --> white 1536
  #13h27w_001.JPG.iris --> white 384
  if(mode==0){
    
    original.filename = just.filename
    
    #first, get if the plate is white or not
    if(str_count(just.filename, "w")>0){
      is.white.plate=T
      #just.filename = gsub(pattern = "w_", replacement = "", just.filename) #for 384 plates
      just.filename = gsub(pattern = "w", replacement = "_", just.filename) #for 1536 plates
      just.filename = gsub(pattern = "__", replacement = "_", just.filename) #for 384 plates
    } else {
      is.white.plate=F
    }
    
    #then, get if it's 1536 or not
    #first, get if the plate is white or not
    if(str_count(just.filename, "_P_")>0){
      is.1536.plate=T
      just.filename = gsub(pattern = "_P", replacement = "", just.filename)
    } else {
      is.1536.plate=F
    }
    
    #now all the plates have this format
    #9h30_001.JPG.iris --> normal 384
    hours = as.integer(my.strsplit(just.filename, 'h', 1))
    minutes = as.integer(my.strsplit(my.strsplit(just.filename, '_', 1), 'h', 2))
    time = hours + minutes/60 #e.g. 9h30 would be 9.5
    
    plate.number = as.integer(my.strsplit(my.strsplit(just.filename, '.', 1), '_', 2))
    
    iris.plate.info = data.frame(plate.number=plate.number,
                                 is.white.plate=is.white.plate,
                                 is.1536.plate=is.1536.plate,
                                 hours=hours,
                                 minutes=minutes,
                                 time=time,
                                 filename=original.filename)
    
    
  }
  
  #Lucia timecourse naming style
  #cocort-27hr-lbnosalt-keio-6_B.JPG.iris
  #Cephalexin-1-5_A.JPG
  if(mode==4){
    plate.split = as.vector(unlist(strsplit(just.filename, split="-")))
    plate.split.again = as.vector(unlist(strsplit(plate.split[length(plate.split)], split="_")))
    plate.split.yet.again = as.vector(unlist(strsplit( plate.split.again[2], split="\\.")))
    
    
    
    iris.plate.info = data.frame(plate.number = as.integer(plate.split.again[1]), 
                                 replicate.number = plate.split.yet.again[1], 
                                 chemical = plate.split[1],
                                 condition = paste(plate.split[1:(length(plate.split)-1)], collapse = "-"),
                                 filename = just.filename)
    
  }
  
  
  #Bsu screen naming style
  #BS023_2_A1.JPG.iris
  if(mode=="BS"){
    plate.split = as.vector(unlist(strsplit(just.filename, split="_")))
    plate.split.again = plate.split[length(plate.split)]
    plate.split.yet.again = as.vector(unlist(strsplit( plate.split.again, split="\\.")))
    
    
    
    iris.plate.info = data.frame(plate.number = unlist(stri_enc_toutf32(substr(plate.split.yet.again[1], 1, 1)))-64, 
                                 replicate.number = stri_enc_fromutf32(as.integer(substr(plate.split.yet.again[1], 2, 2))+64), 
                                 chemical = plate.split[1],
                                 condition = paste(plate.split[1:(length(plate.split)-1)], collapse = "-"),
                                 filename = just.filename)
  }
  
  
  #2 libraries Bsu data from Byoung-Mo
  #BKE_spo9_5_C2.JPG.iris
  if(mode=="BS2"){
    plate.split = as.vector(unlist(strsplit(just.filename, split="_")))
    plate.split.again = plate.split[length(plate.split)]
    plate.split.yet.again = as.vector(unlist(strsplit( plate.split.again, split="\\.")))
    
    
    
    iris.plate.info = data.frame(plate.number = unlist(stri_enc_toutf32(substr(plate.split.yet.again[1], 1, 1)))-64, 
                                 replicate.number = stri_enc_fromutf32(as.integer(substr(plate.split.yet.again[1], 2, 2))+64), 
                                 chemical = plate.split[1],
                                 condition = paste(plate.split[1:(length(plate.split)-1)], collapse = "-"),
                                 filename = just.filename)
  }
  
  
  
  
  
  #Candida naming style
  #Ca_15day7.JPG.iris
  if(mode=="Ca"){
    original.filename = just.filename
    
    just.filename = my.strsplit(just.filename, delimiter = ".", column = 1)
    just.filename = my.strsplit(just.filename, delimiter = "_", column = 2)
    plate.number = as.integer(my.strsplit(just.filename, delimiter = "day", column = 1))
    day = as.integer(my.strsplit(just.filename, delimiter = "day", column = 2))
    
    
    
    iris.plate.info = data.frame(plate.number = plate.number,
                                 day = day,
                                 filename = original.filename)
  }
  
  
  
  #timecourse 2015 naming style
  #plate_001-15-08-12_11-05-23.JPG
  if(mode=="timecourse2"){
    original.filename = just.filename
    
    just.filename = my.strsplit(just.filename, delimiter = "timecourse_", column = 2)
    just.filename = my.strsplit(just.filename, delimiter = ".", column = 1)
    plate.number = as.integer(my.strsplit(just.filename, delimiter = "-", column = 1))
    time.string = my.strsplit(just.filename, delimiter = "_", column = 2)
    time.hour = as.integer(my.strsplit(time.string, delimiter = "-", column = 1))
    time.minute = as.integer(my.strsplit(time.string, delimiter = "-", column = 2))
    time.decimal = time.hour + time.minute/60
    
    
    
    iris.plate.info = data.frame(plate.number = plate.number,
                                 time.of.day = time.decimal,
                                 filename = original.filename)
  }
  
  "TransBac_008-17-41.JPG.iris"
  if(mode=="timecourse3"){
    original.filename = just.filename
    
    just.filename = my.strsplit(just.filename, delimiter = "_", column = 2)
    just.filename = my.strsplit(just.filename, delimiter = ".", column = 1)
    plate.number = as.integer(my.strsplit(just.filename, delimiter = "-", column = 1))
    time.hour = as.integer(my.strsplit(just.filename, delimiter = "-", column = 2))
    time.minute = as.integer(my.strsplit(just.filename, delimiter = "-", column = 3))
    time.decimal = time.hour + time.minute/60
    
    
    
    iris.plate.info = data.frame(plate.number = plate.number,
                                 time.of.day = time.decimal,
                                 filename = original.filename)
  }
  
  
  
  return(iris.plate.info) 
}






loadIrisFile.size.add.meta.info <- function(file.path, filename.mode="auto"){
  # reads in the values from the specified iris file, including its metadata
  #
  # Args:
  #   file.path:              The path to an iris file on disk
  #   column.name:            which column(s) to normalize by size
  #
  # Returns:
  #   all the data included in the iris file, including the meta-data
  #
  # Author:   George Kritikos
  # Date:     10.12.2014
  # Contact:  kritikos@embl.de
  #
  
  #install needed packages (if not already installed)
  
  
  
  
  #read in the iris file plus metadata
  if(grepl("iris$",file.path)){#if it's an iris file
    iris.file.data = read.table(file.path, header=T, sep="\t", skip=6)
  } else if(grepl("dat$",file.path)){#if it's a dat file
    iris.file.data = read.table(file.path, header=T, sep="", skip=0)
    colnames(iris.file.data)<-c("row", "column", "size", "circularity")
  }
  
  iris.plate.info = get.iris.plate.info(file.path, mode=filename.mode)
  iris.file.data = cbind(iris.file.data, iris.plate.info)
  iris.file.data$unique.mutant.id = iris.file.data$plate.number*10^4 + iris.file.data$row*10^2 + iris.file.data$column 
  
  
  return(iris.file.data)
}



#splits strings (or vectors of strings) into vectors of split strings.
#can also extract the exact string to extract (e.g. after the 2nd delimiter)
my.strsplit <- function(string.vector, delimiter, column=NA){
  if(is.na(column)){
    return(unlist(strsplit(as.character(string.vector), split=delimiter,fixed=TRUE)))
  } else {
    return(sapply(strsplit(as.character(string.vector), split=delimiter,fixed=TRUE), "[[", column))
  }
}
