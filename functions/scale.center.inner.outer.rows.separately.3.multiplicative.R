

scale.center.inner.outer.rows.separately.3.multiplicative <- function(plateData1D, RCs.to.correct = 4, correct.inner.plate = F, correct.corner.twice = F){
  
  format = length(plateData1D)
  
  if(format==96){
    max.row.number = 8
    max.column.number = 12
  }
  #R didn't really like 'else-if' statements, like any other programming language
  if(format==384){
    max.row.number = 16
    max.column.number = 24
  }
  if(format==1536){
    max.row.number = 32
    max.column.number = 48
  }
  if(format==6144){
    max.row.number = 64
    max.column.number = 96
  }
  
  if(!(format==6144 || format==1536 || format==384 || format==96)){
    return(rep(NA,format)) #return the same size as the input matrix but filled with NAs instead
  }
  
  #convert to a data table
  rc.info = expand.grid(1:max.column.number, 1:max.row.number)
  plate.data = data.table(data.frame(original.values=plateData1D), row=rc.info$Var2, column=rc.info$Var1)
  
  #target values are inner plate values (e.g. exclude the columns to correct)
  target.values = plate.data[!row %in% c(1:RCs.to.correct,(1+max.row.number-RCs.to.correct):max.row.number)][!column %in% c(1:RCs.to.correct,(1+max.column.number-RCs.to.correct):max.column.number)]$original.values
  
  
  plate.data[,rc.corrected:=NA_real_]
  
  if(!correct.corner.twice){
    #i is the offset from the outermost row and column
    #only correct uncorrected values
    
    for(i in c(0:(RCs.to.correct-1))){
      plate.data[column==(1+i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[column==(max.column.number-i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      
      plate.data[row==(1+i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[row==(max.row.number-i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
    }
    
    # for(i in c(0:(RCs.to.correct-1))){
    #   plate.data[column==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    #   plate.data[column==(max.column.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    #   
    #   plate.data[row==(1+i) & column>RCs.to.correct & column<=(max.column.number-RCs.to.correct), rc.corrected:=scale.multiplicative(original.values, target.values)]
    #   plate.data[row==(max.row.number-i) & column>RCs.to.correct & column<=(max.column.number-RCs.to.correct), rc.corrected:=scale.multiplicative(original.values, target.values)]
    # }
  } else {
    for(i in c(0:(RCs.to.correct-1))){
      plate.data[column==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[column==(max.column.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      
      plate.data[row==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[row==(max.row.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    }
  }
  
  
  
  #copy over middle plate values
  plate.data[column>RCs.to.correct & column<=(max.column.number-RCs.to.correct) & row>RCs.to.correct & row<=(max.row.number-RCs.to.correct),
             rc.corrected:=original.values]
  
  
  return(plate.data$rc.corrected)
  
}