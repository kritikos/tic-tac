
surface.outerRC.correction<-function(plateData1D, 
                                     rows.columns.to.correct=2, 
                                     surface.correct.as.well=T,
                                     correct.inner.plate = F,
                                     correct.corner.twice=F){
  
  format = length(plateData1D)
  
  if(format==96){
    max.row.number = 8
    max.column.number = 12
  }
  #R didn't really like 'else-if' statements, like any other programming language
  if(format==384){
    max.row.number = 16
    max.column.number = 24
  }
  if(format==1536){
    max.row.number = 32
    max.column.number = 48
  }
  if(format==6144){
    max.row.number = 64
    max.column.number = 96
  }
  
  if(!(format==6144 || format==1536 || format==384 || format==96)){
    return(rep(NA_real_,format)) #return the same size as the input matrix but filled with NAs instead
  }
  
  #convert to a data table
  rc.info = expand.grid(1:max.column.number, 1:max.row.number)
  plate.data = data.table(data.frame(original.values.before.outer.RC=plateData1D), row=rc.info$Var2, column=rc.info$Var1)
  
  
  #first correct the outer RCs
  plateData1D = scale.center.inner.outer.rows.separately.3.multiplicative(plateData1D = plateData1D,
                                                                          RCs.to.correct = rows.columns.to.correct, 
                                                                          correct.inner.plate = correct.inner.plate, 
                                                                          correct.corner.twice = correct.corner.twice)
  
  plate.data[,original.values := plateData1D]
  
  #surface correct but remove X outer columns
  suppressWarnings(plate.data[,input.to.surface.correct:=NULL])
  plate.data[,input.to.surface.correct:=as.double(original.values)]
  
  for(i in c(0:(rows.columns.to.correct-1))){
    plate.data[column==(1+i), input.to.surface.correct:=NA]
    plate.data[column==(max.column.number-i), input.to.surface.correct:=NA]
  }
  
  for(i in c(0:(rows.columns.to.correct-1))){
    plate.data[row==(1+i), input.to.surface.correct:=NA]
    plate.data[row==(max.row.number-i), input.to.surface.correct:=NA]
  }
  
  #do the surface correction
  plate.data[,surface.corrected:=correctSurface.multiplicative(input.to.surface.correct, do.post.corrections = F)]
  
  #put back the X outer columns, as they've been already corrected for (multiplicative)
  for(i in c(0:(rows.columns.to.correct-1))){
    plate.data[column==(1+i), surface.corrected:=original.values]
    plate.data[column==(max.column.number-i), surface.corrected:=original.values]
    
    plate.data[row==(1+i), surface.corrected:=original.values]
    plate.data[row==(max.row.number-i), surface.corrected:=original.values]
  }
  
  return(plate.data$surface.corrected)
}


scale.center.inner.outer.rows.separately.3.multiplicative <- function(plateData1D, RCs.to.correct = 4, correct.inner.plate = F, correct.corner.twice = F){
  
  format = length(plateData1D)
  
  if(format==96){
    max.row.number = 8
    max.column.number = 12
  }
  #R didn't really like 'else-if' statements, like any other programming language
  if(format==384){
    max.row.number = 16
    max.column.number = 24
  }
  if(format==1536){
    max.row.number = 32
    max.column.number = 48
  }
  if(format==6144){
    max.row.number = 64
    max.column.number = 96
  }
  
  if(!(format==6144 || format==1536 || format==384 || format==96)){
    return(rep(NA,format)) #return the same size as the input matrix but filled with NAs instead
  }
  
  #convert to a data table
  rc.info = expand.grid(1:max.column.number, 1:max.row.number)
  plate.data = data.table(data.frame(original.values=plateData1D), row=rc.info$Var2, column=rc.info$Var1)
  
  #target values are inner plate values (e.g. exclude the columns to correct)
  target.values = plate.data[!row %in% c(1:RCs.to.correct,(1+max.row.number-RCs.to.correct):max.row.number)][!column %in% c(1:RCs.to.correct,(1+max.column.number-RCs.to.correct):max.column.number)]$original.values
  
  
  plate.data[,rc.corrected:=NA_real_]
  
  if(!correct.corner.twice){
    #i is the offset from the outermost row and column
    #only correct uncorrected values
    
    for(i in c(0:(RCs.to.correct-1))){
      plate.data[column==(1+i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[column==(max.column.number-i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      
      plate.data[row==(1+i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[row==(max.row.number-i) & is.na(rc.corrected), rc.corrected:=scale.multiplicative(original.values, target.values)]
    }
    
    ' 
    for(i in c(0:(RCs.to.correct-1))){
    plate.data[column==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    plate.data[column==(max.column.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    
    plate.data[row==(1+i) & column>RCs.to.correct & column<=(max.column.number-RCs.to.correct), rc.corrected:=scale.multiplicative(original.values, target.values)]
    plate.data[row==(max.row.number-i) & column>RCs.to.correct & column<=(max.column.number-RCs.to.correct), rc.corrected:=scale.multiplicative(original.values, target.values)]
    }
    '
  } else {
    for(i in c(0:(RCs.to.correct-1))){
      plate.data[column==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[column==(max.column.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      
      plate.data[row==(1+i), rc.corrected:=scale.multiplicative(original.values, target.values)]
      plate.data[row==(max.row.number-i), rc.corrected:=scale.multiplicative(original.values, target.values)]
    }
  }
  
  
  
  #copy over middle plate values
  plate.data[column>RCs.to.correct & column<=(max.column.number-RCs.to.correct) & row>RCs.to.correct & row<=(max.row.number-RCs.to.correct),
             rc.corrected:=original.values]
  
  
  return(plate.data$rc.corrected)
  
}



correctSurface.multiplicative<-function(vector,d=2, mode=length(vector), do.post.corrections=F){
  
  if(mode==1536)
    df<-data.frame(row=rep(1:32,each=48),column=rep(1:48,32),variable=vector)
  
  if(mode==384)
    df<-data.frame(row=rep(1:16,each=24),column=rep(1:24,16),variable=vector)
  
  if(mode==96)
    df<-data.frame(row=rep(1:8,each=12),column=rep(1:12,8),variable=vector)
  
  
  #calculate the 2D surface
  #   rm(linear.model)
  linear.model = lm(variable ~ poly(row, column, degree=d), data=df)
  #   rm(prediction)
  prediction = predict.lm(linear.model, df)
  
  #get the surface corrected values, set the plate mean
  df$variable.surface.corrected = (df$variable/prediction) * mean(vector,na.rm=T)
  
  #make sure nothing is negative after the corrections
  if(do.post.corrections){
    df$variable.surface.corrected[df$variable.surface.corrected<0]=0
    
    #if it was zero before, make sure the surface correction didn't set it to another value
    df$variable.surface.corrected[df$variable==0]=0
  }
  
  
  return(df$variable.surface.corrected)
  
}

