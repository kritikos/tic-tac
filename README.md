# TIC/TAC

All the data analysis steps performed in the context of the   
_Phage proteins block and trigger retron toxin/antitoxin systems_ paper by Bobonis et al.  


1. Load iris files
2. Flag colonies with low opacity and load gene names
3. Apply multiplicative correction for outer Rows and Columns (RC) effects
4. Visualize multiplicative correction
5. Calculate z-scores
6. Visualize z-score correction
7. Replicate reproducibility
8. Get average z-score across replicates
9. Get z-score differences
10. Plot according to genomic locations
11. Plot z-score-diff distributions and hit heatmaps
12. Plot replicate reproducibility scatter plots with hits annotated
13. Calculating p-values and saving tables
14. Prophage gene enrichment
  
  
For more technical info on these steps, please visit the document
`tic_tac_report.Rmd`.  
  

For feedback, and questions, please contact:  
George Kritikos  
kritikos@embl.de  
